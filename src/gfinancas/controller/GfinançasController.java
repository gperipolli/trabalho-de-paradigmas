/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfinancas.controller;

import gfinancas.model.GfinançasModel;
import gfinancas.model.JcomboBoxModel;
import gfinancas.model.Movimentação;
import gfinancas.model.TableModelMovimentações;
import java.util.Date;


/**
 *
 * @author guilherme
 */
public class GfinançasController {
    private GfinançasModel model;
    
    public GfinançasController ()
    {
        model = new GfinançasModel();
    }

    public TableModelMovimentações get_table()
    {
        return model.get_table();
    }
    
    public JcomboBoxModel get_tipos()
    {
        return model.get_tipos();
    }
    
    public double get_saldo_model ()
    {
        return model.getSaldo();
    }

    public void atualiza_saldo (double valor)
	{
		model.setSaldo(model.getSaldo() + valor);
	}
    public void add_tipo (String tipo)
    {
        model.add_tipo(tipo);
    }

    public Movimentação cria_mov (String tipo, String descricao, double valor)
    {
        Date data = new Date(System.currentTimeMillis());
        Movimentação mov = new Movimentação(tipo, descricao, data, valor);
        return mov;
    }
    public void insere_mov (String tipo, String descricao, double valor)
    {
        model.add_tipo(tipo);
        Movimentação mov = cria_mov(tipo, descricao, valor);
        model.add_mov(mov);
        atualiza_saldo(valor);
    }
    

    public void remove_mov (int index,double valor)
    {
        model.remove_mov(index);
        atualiza_saldo(valor*-1);
    }
    public void save ()
    {
        model.save();
    }
          
}
