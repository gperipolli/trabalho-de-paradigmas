/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gfinancas.model;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author guilherme
 */
public class GfinançasModel {
    private double saldo;
    private TableModelMovimentações tableModelMov;
    private JcomboBoxModel tipos;
    
    public GfinançasModel()
    {
        tableModelMov = new TableModelMovimentações();
        tipos = new JcomboBoxModel();
        saldo = 0;
    }
    
    public double getSaldo() {
    	return saldo;
    }
    public void add_tipo (String tipo)
    {
        if (!tipos.existe_tipo(tipo))
            tipos.add_tipo(tipo);
    }

    public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
    
    
    public TableModelMovimentações get_table()
    {
        return tableModelMov;
    }
    
    public JcomboBoxModel get_tipos()
    {
        return tipos;
    }
    
    public void add_mov (Movimentação mov)
    {
        tableModelMov.add(mov);
    }
    public void remove_mov (int index)
    {
        tableModelMov.remove(index);
    }   
    
    public void save ()
    {
        try {
            ArrayList<Movimentação> movs = tableModelMov.get_array();
            ArrayList<String> arrayTipos = tipos.get_tipos();
            EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("GfinancasPU");
            EntityManager em = managerFactory.createEntityManager();
            EntityTransaction tran = em.getTransaction();
            tran.begin();
            for (Movimentação m : movs)
            {
                em.persist(m);
                tran.commit();
}
            em.close();
            
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
                
                    
                
    }
}
