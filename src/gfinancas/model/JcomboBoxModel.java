/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfinancas.model;

import java.util.ArrayList;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author guilherme
 */
public class JcomboBoxModel implements ComboBoxModel {
    private ArrayList<String> tiposdefinidos;
    private String tipo;
    private String[] tpadroes;
    private String[] refreshsaldo;
    private ArrayList<String> refreshersaldo;

    public JcomboBoxModel() {
        this.tpadroes = new String[]{"Outro","Salário","Alimentação", "Água", "Luz", "Condomínio","Combustível" ,"Diversos"};
        tiposdefinidos = new ArrayList<>();
        for (String t : tpadroes)
        {
            tiposdefinidos.add(t);
        }
        
     }
    public ArrayList<String> get_tipos()
            {
                return tiposdefinidos;
            }
    
    @Override
    public void setSelectedItem(Object anItem) {
        tipo = anItem.toString();
    }
    public boolean existe_tipo (String tipo)
    {
        int index = tiposdefinidos.indexOf(tipo);
        if (index == -1)
            return false;
        else
            return true;
    }

    @Override
    public Object getSelectedItem() {
        return tipo;
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void add_tipo (String tipo)
    {
        tiposdefinidos.add(tipo);
    }

    @Override
    public int getSize() {
        return tiposdefinidos.size();
    }

    @Override
    public Object getElementAt(int index) {
        
        return tiposdefinidos.get(index);
        
    }

    @Override
    public void addListDataListener(ListDataListener l) {

    }

    @Override
    public void removeListDataListener(ListDataListener l) {

    }
    
}
