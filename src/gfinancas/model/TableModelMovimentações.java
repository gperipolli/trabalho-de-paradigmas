/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfinancas.model;

import java.awt.SystemColor;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author guilherme
 */
public class TableModelMovimentações extends AbstractTableModel {

    private ArrayList<Movimentação> movs;
    
    private static final String[] columnNames = {"Tipo", "Descrição", "Data", "Valor"};
    
    public TableModelMovimentações ()
    {
        movs = new ArrayList<>();
    }
    public int getIndex (Movimentação mov)
    {
        return movs.indexOf(mov);
    }
    public ArrayList<Movimentação> get_array ()
    {
        return movs;
    }
    
    @Override
    public int getRowCount() {
        return movs.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
      @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex) {
            case 0: return movs.get(rowIndex).get_tipo();
            case 1: return movs.get(rowIndex).get_descricao();
            case 2: return movs.get(rowIndex).get_data();
            case 3: return movs.get(rowIndex).get_valor();
        }
        return null;
    }
    
    public void add(Movimentação mov) {
        
        this.movs.add(mov);
        fireTableRowsInserted(this.movs.size()-1, this.movs.size()-1);
    }
    
     public void remove(int index) {
         
        movs.remove(index);
	fireTableRowsDeleted(index, index);
    }
     
     public void update(int index, Movimentação mov) {
        this.movs.set(index, mov);
        fireTableRowsUpdated(index, index);
    }
     
     public Movimentação select(int index) {
        return movs.get(index);
        
    }
    
}
