/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfinancas.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author guilherme
 */
@Entity
public class Movimentação implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private static final SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy : hh:mm:ss");
    private double valor;
    private String tipo;
    private String descricao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data;

    public Movimentação() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movimentação)) {
            return false;
        }
        Movimentação other = (Movimentação) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gfinancas.model.Movimenta\u00e7\u00e3o[ id=" + id + " ]";
    }
    
    public Movimentação (String tipo, String descricao, Date data, double valor)
    {
        this.valor = valor;
        this.tipo = tipo;
        this.data = data;
        this. descricao = descricao;
    }
   
    public String get_tipo ()
        {
            return tipo;
        }
    public String get_descricao ()
    {
        return descricao;
    }
    public String get_valor()
    {
        return Double.toString(valor);
    }

    public String get_data ()
    {
        return formato.format(data);
    }
    
}
